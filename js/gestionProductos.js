var productosObtenidos;


function getProducts(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){

    if (this.readyState == 4 && this.status == 200){
        productosObtenidos = request.responseText;
        procesarProductos();
    }
  }

  request.open("GET",url,true);
  request.send();

}

function procesarProductos(){
  var JSONProductos = JSON.parse(productosObtenidos);
  var divTabla = document.getElementById("tablaProdc");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  var headerNombre = document.createElement("th");
  headerNombre.innerText =  "NOMBRE";

  var headerPrecio = document.createElement("th");
  headerPrecio.innerText =  "PRECIO";

  var headerStock = document.createElement("th");
  headerStock.innerText =  "CANTIDAD";

  tabla.appendChild(headerNombre);
  tabla.appendChild(headerPrecio);
  tabla.appendChild(headerStock);

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var x in JSONProductos.value) {
    //console.log(JSONProductos.value[x].ProductName);
    var fila  = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText =  JSONProductos.value[x].ProductName;

    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText =  JSONProductos.value[x].UnitPrice;

    var columnaStock = document.createElement("td");
    columnaStock.innerText =  JSONProductos.value[x].UnitsInStock;

    fila.appendChild(columnaNombre);
    fila.appendChild(columnaPrecio);
    fila.appendChild(columnaStock);

    tbody.appendChild(fila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
