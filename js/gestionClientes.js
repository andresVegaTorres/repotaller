var clientesObtenidos;

function getCustomers(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){

    if (this.readyState == 4 && this.status == 200){
        //console.log(request.responseText);
        clientesObtenidos = request.responseText;
        procesarClientes();
    }
  }

  request.open("GET",url,true);
  request.send();

}

function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);
  var divTabla = document.getElementById("tablaProdc");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  var urlBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";

  var headerNombre = document.createElement("th");
  headerNombre.innerText =  "NOMBRE";

  var headerCargo = document.createElement("th");
  headerCargo.innerText =  "CARGO";

  var headerCiudad = document.createElement("th");
  headerCiudad.innerText =  "CIUDAD";

  var headerImage = document.createElement("th");
  headerImage.innerText =  "BANDERA";

  tabla.appendChild(headerNombre);
  tabla.appendChild(headerCargo);
  tabla.appendChild(headerCiudad);
  tabla.appendChild(headerImage);

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var x in JSONClientes.value) {
    //console.log(JSONProductos.value[x].ProductName);
    var fila  = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText =  JSONClientes.value[x].ContactName;

    var columnaCargo = document.createElement("td");
    columnaCargo.innerText =  JSONClientes.value[x].ContactTitle;

    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText =  JSONClientes.value[x].Country;

    var columnaBandera = document.createElement("td");
    var imgBandera = document.createElement("img");

    if (JSONClientes.value[x].Country == "UK"){
      imgBandera.src = urlBandera+"United-Kingdom.png";
    }else{
      imgBandera.src = urlBandera+JSONClientes.value[x].Country+".png";
    }

    imgBandera.classList.add("flag");
    columnaBandera.appendChild(imgBandera);

    fila.appendChild(columnaNombre);
    fila.appendChild(columnaCargo);
    fila.appendChild(columnaCiudad);
    fila.appendChild(columnaBandera);

    tbody.appendChild(fila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
